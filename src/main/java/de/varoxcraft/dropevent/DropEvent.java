package de.varoxcraft.dropevent;

import de.varoxcraft.dropevent.commands.DropEventCommand;
import de.varoxcraft.dropevent.listener.DropEventArmorStandProtectionListener;
import de.varoxcraft.dropevent.listener.InventoryCloseListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.ArmorStand;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class DropEvent extends JavaPlugin {

    @Getter
    private String prefix;
    @Getter
    private static DropEvent instance;
    @Getter
    private static String version;

    @Override
    public void onEnable() {
        instance = this;
        prefix = "§8[§e§lDrop-Event§8] §7";
        Bukkit.getConsoleSender().sendMessage(getPrefix() + "§eVersuche das Plugin " + getName() + " zu laden...");
        version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        Bukkit.getConsoleSender().sendMessage(getPrefix() + "§eVersion: §l" + version);

        getCommand("dropevent").setExecutor(new DropEventCommand());
        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new InventoryCloseListener(), this);
        pluginManager.registerEvents(new DropEventArmorStandProtectionListener(), this);

        Bukkit.getConsoleSender().sendMessage(getPrefix() + "§aDas Plugin " + getName() + " wurde geladen!");
    }

    @Override
    public void onDisable() {
        for(ArmorStand armorStand : EventObject.getArmorStands()){
            armorStand.remove();
        }
    }
}
