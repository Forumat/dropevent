package de.varoxcraft.dropevent;

import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class Cooldown {

    private HashMap<UUID, Long> cooldowns;
    private String bypassPerm = null;

    public Cooldown(final String bypassPerm){
        cooldowns = Maps.newHashMap();
        this.bypassPerm = bypassPerm;
    }

    public static String convertSekToMin(final Integer seconds){
        if(seconds >= 60){
            double minutes = seconds*0.0166667;
            minutes = Math.round(minutes);
            return (int)minutes + " Minuten";
        }else{
            return seconds + " Sekunden";
        }
    }

    public Cooldown(){
        cooldowns = Maps.newHashMap();
    }

    public Cooldown addPlayerToCooldown(final UUID uuid, final Integer seconds){
        cooldowns.put(uuid, System.currentTimeMillis() + (seconds*1000));
        return this;
    }

    public Cooldown removePlayerFromCooldown(final UUID uuid){
        if(cooldowns.containsKey(uuid))
            cooldowns.remove(uuid);
        return this;
    }

    public boolean isDone(final UUID uuid){
        if(cooldowns.containsKey(uuid)){
            if(cooldowns.get(uuid) <= System.currentTimeMillis() || Bukkit.getPlayer(uuid).hasPermission(bypassPerm)){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    public Integer getReminding(final UUID uuid){
        if(cooldowns.containsKey(uuid)){
            return Math.toIntExact((cooldowns.get(uuid) - System.currentTimeMillis()) / 1000);
        }else{
            return null;
        }
    }

    public Cooldown addPlayerToCooldown(final Player player, final Integer seconds){
        if(bypassPerm != null){
            if(!player.hasPermission(this.bypassPerm)){
                addPlayerToCooldown(player.getUniqueId(), seconds);
            }
        }else{
            addPlayerToCooldown(player.getUniqueId(), seconds);
        }
        return this;
    }

    public Cooldown removePlayerFromCooldown(final Player player){
        removePlayerFromCooldown(player.getUniqueId());
        return this;
    }

    public boolean isDone(final Player player){
        return isDone(player.getUniqueId());
    }

    public Integer getReminding(final Player player){
        return getReminding(player.getUniqueId());
    }


}
