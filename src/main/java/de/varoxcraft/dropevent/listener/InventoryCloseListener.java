package de.varoxcraft.dropevent.listener;

import de.varoxcraft.dropevent.Cooldown;
import de.varoxcraft.dropevent.DropEvent;
import de.varoxcraft.dropevent.EventObject;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InventoryCloseListener implements Listener {

    Cooldown commandCooldown = new Cooldown("varoxcraft.dropevent.cooldown.bypass");

    @EventHandler
    public void onClose(InventoryCloseEvent event){
        if(!event.getInventory().getTitle().equalsIgnoreCase("§e§lDrop-Event")) return;
        if(!isValid(event.getInventory())) {
            event.getPlayer().sendMessage(DropEvent.getInstance().getPrefix() + "§cDu musst mindestens 5 Items in das Inventar legen!");
            for(ItemStack itemStack : event.getInventory().getContents()){
                if (itemStack != null && itemStack.getType() != Material.AIR) {
                    event.getPlayer().getInventory().addItem(itemStack);
                }
            }
            return;
        }
        if(!commandCooldown.isDone(event.getPlayer().getUniqueId())){
            for(ItemStack itemStack : event.getInventory().getContents()){
                if (itemStack != null && itemStack.getType() != Material.AIR) {
                    event.getPlayer().getInventory().addItem(itemStack);
                }
            }
            event.getPlayer().sendMessage(DropEvent.getInstance().getPrefix() + "§cDu musst noch §e" + Cooldown.convertSekToMin(commandCooldown.getReminding(event.getPlayer().getUniqueId())) + "§c warten bis du ein weiteres Drop-Event starten kannst.");
            return;
        }
        commandCooldown.addPlayerToCooldown(event.getPlayer().getUniqueId(), 120);
        EventObject eventObject = new EventObject((Player )event.getPlayer(), event.getInventory().getContents(), event.getPlayer().getEyeLocation(), new ArrayList<>());
        eventObject.startEvent();

    }


    private boolean isValid(Inventory inventory) {
        ItemStack[] contents = inventory.getContents();
        List<ItemStack> contentList = Arrays.asList(contents);
        List<ItemStack> realItems = new ArrayList<>();
        for(ItemStack itemStack : contentList){
            if(itemStack != null && itemStack.getType() != Material.AIR){
                realItems.add(itemStack);
            }
        }
        if(realItems.size() >= 5){
            return true;
        }
        return false;
    }

}
