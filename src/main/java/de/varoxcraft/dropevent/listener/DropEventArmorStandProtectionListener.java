package de.varoxcraft.dropevent.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

public class DropEventArmorStandProtectionListener implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event){
        if(event.getEntity().getType() == EntityType.ARMOR_STAND){
            if(event.getEntity().getCustomName().contains("§e§lDROP-EVENT")){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onManipulate(PlayerArmorStandManipulateEvent event){
        if(event.getRightClicked().getCustomName().contains("§e§lDROP-EVENT")){
            event.setCancelled(true);
        }
    }

}
