package de.varoxcraft.dropevent;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.Validate;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.*;


@RequiredArgsConstructor
public class EventObject {

    @NonNull
    @Getter
    private Player started;
    @NonNull
    @Getter
    private ItemStack[] itemStacks;
    @NonNull
    @Getter
    private Location startLocation;
    @Getter @NonNull
    public List<Item> items;
    @Getter
    private ArmorStand armorStand;
    @Getter
    private static List<ArmorStand> armorStands = new ArrayList<>();
    @Getter
    private static EventObject currentDropEvent = null;

    int taskId;
    BukkitTask runnable;

    private void startSpinning(){
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(DropEvent.getInstance(), () ->{
            Location location = armorStand.getLocation();
            location.setYaw(armorStand.getLocation().getYaw() + 5F);
            armorStand.teleport(location);
        }, 20*5, 1);
    }

    public void startEvent(){


        armorStand = (ArmorStand) startLocation.getWorld().spawnEntity(startLocation.subtract(0,3,0), EntityType.ARMOR_STAND);
        armorStand.setGravity(false);
        armorStand.setAI(false);
        armorStand.setCustomName("§e§lDROP-EVENT §8» §e" + started.getDisplayName());
        armorStand.setCustomNameVisible(true);
        armorStand.setVisible(false);
        armorStand.setHelmet(new ItemStack(Material.CHEST));
        armorStands.add(armorStand);
        final Location oldLocation = startLocation;
        startSpinning();
        runnable = Bukkit.getScheduler().runTaskTimer(DropEvent.getInstance(), () ->{
            if(oldLocation.getY() + 5 > armorStand.getLocation().getY()){
                Location location = armorStand.getLocation().add(0,0.1,0);
                armorStand.teleport(location);
            }else{
                startExplosion();
                runnable.cancel();

            }
        }, 20*5, 1);




    }

    BukkitTask bukkitTask;
    boolean firstExplode = true;
    private void startExplosion(){

        List<ItemStack> itemStacks1 = new ArrayList<>();
            for(ItemStack itemStack : itemStacks){
                if(itemStack != null && itemStack.getType() != null && itemStack.getType() != Material.AIR) {
                    if (itemStack.getAmount() > 1) {
                        int amount  = itemStack.getAmount();
                        for (int i = 0; i < amount; i++) {
                            itemStack.setAmount(1);
                            itemStacks1.add(itemStack);
                        }
                    } else {
                        itemStacks1.add(itemStack);
                    }
                }
            }
        Collections.shuffle(itemStacks1);

       bukkitTask = Bukkit.getScheduler().runTaskTimer(DropEvent.getInstance(), () -> {
           if(firstExplode){
               firstExplode = false;
               startLocation.getWorld().playEffect(armorStand.getLocation().add(0,2,0), Effect.EXPLOSION_HUGE, 1, 10);
           }else{
               startLocation.getWorld().playEffect(startLocation, Effect.ENDEREYE_LAUNCH, 1, 25);

               for(Location location : getCircle(armorStand.getLocation().add(0, 1.9, 0), 0.5, 30)){
                   location.getWorld().playEffect(location, Effect.CLOUD, 1, 8);
               }
           }
           if(itemStacks1.size() == 0){
               getArmorStand().getLocation().getWorld().playEffect(getArmorStand().getLocation().add(0,2,0), Effect.ENDER_SIGNAL, 1, 25);
               Bukkit.getScheduler().cancelTask(taskId);
               getArmorStand().remove();
               currentDropEvent = null;
               bukkitTask.cancel();
           }else if(itemStacks1.size() == 2){
               for(int i = 0; i < 2; i++) {
                   Item item = startLocation.getWorld().dropItem(armorStand.getLocation().add(0,2,0), itemStacks1.get(i));
                   itemStacks1.remove(itemStacks1.get(i));
                   Vector direction = new Vector();
                   direction.setX(0.0D + Math.random() - Math.random());
                   direction.setZ(0.0D + Math.random() - Math.random());
                   direction.setY(0.4D);
                   item.setVelocity(direction.multiply(1.5));
                   startLocation.getWorld().playSound(startLocation, Sound.BLOCK_STONE_BUTTON_CLICK_ON,2,2);
               }
           }else{
               Item item = startLocation.getWorld().dropItem(armorStand.getLocation().add(0,2,0), itemStacks1.get(0));
               itemStacks1.remove(itemStacks1.get(0));
               Vector direction = new Vector();
               direction.setX(0.0D + Math.random() - Math.random());
               direction.setZ(0.0D + Math.random() - Math.random());
               direction.setY(0.4D);
               item.setVelocity(direction.multiply(1.5));
           }

       }, 20*2, 5);

    }

    private ArrayList<Location> getCircle(Location center, double radius, int amount)
    {
        World world = center.getWorld();
        double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        for(int i = 0;i < amount; i++)
        {
            double angle = i * increment;
            double x = center.getX() + (radius * Math.cos(angle));
            double z = center.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(world, x, center.getY(), z));
        }
        return locations;
    }


}
